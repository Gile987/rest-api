-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 18, 2018 at 10:33 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rest_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `konverzije`
--

DROP TABLE IF EXISTS `konverzije`;
CREATE TABLE IF NOT EXISTS `konverzije` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `iznos` float NOT NULL,
  `valuta_kod` varchar(3) NOT NULL,
  `iznos_kursa` float NOT NULL,
  `konvertovani_iznos` decimal(12,2) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `konverzije`
--

INSERT INTO `konverzije` (`id`, `iznos`, `valuta_kod`, `iznos_kursa`, `konvertovani_iznos`, `timestamp`) VALUES
(1, 100, 'AUD', 75.6132, '7561.32', '2018-06-18 09:10:44'),
(2, 100, 'GBP', 134.821, '13482.10', '2018-06-18 09:10:52'),
(3, 350, 'EUR', 117.821, '41237.35', '2018-06-18 09:10:57'),
(4, 333, 'CAD', 77.0975, '25673.47', '2018-06-18 09:13:22'),
(5, 666666, 'CHF', 101.912, '67941265.39', '2018-06-18 09:42:23'),
(6, 235, 'EUR', 117.821, '27687.94', '2018-06-18 09:44:42'),
(7, 334, 'CAD', 77.0975, '25750.57', '2018-06-18 09:45:17'),
(8, 34, 'USD', 101.604, '3454.54', '2018-06-18 09:45:42'),
(9, 33333, 'GBP', 134.821, '4493988.39', '2018-06-18 09:45:57'),
(10, 111, 'CAD', 77.0975, '8557.82', '2018-06-18 09:55:11'),
(11, 34, 'USD', 101.91, '3464.94', '2018-06-18 10:00:32'),
(12, 234234, 'CAD', 77.5615, '18167540.39', '2018-06-18 10:00:39'),
(13, 333, 'CAD', 77.0975, '25673.47', '2018-06-18 10:01:10'),
(14, 333, 'CAD', 77.3295, '25750.72', '2018-06-18 10:01:17'),
(15, 333, 'CAD', 77.5615, '25827.98', '2018-06-18 10:01:24'),
(16, 33, 'USD', 102.216, '3373.13', '2018-06-18 10:31:08'),
(17, 12, 'GBP', 135.227, '1622.72', '2018-06-18 10:31:15'),
(18, 222, 'CHF', 102.525, '22760.55', '2018-06-18 10:31:21');

-- --------------------------------------------------------

--
-- Table structure for table `kurs_dinara`
--

DROP TABLE IF EXISTS `kurs_dinara`;
CREATE TABLE IF NOT EXISTS `kurs_dinara` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `valuta_id` int(11) NOT NULL,
  `kupovni` float NOT NULL,
  `srednji` float NOT NULL,
  `prodajni` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kurs_dinara`
--

INSERT INTO `kurs_dinara` (`id`, `datum`, `valuta_id`, `kupovni`, `srednji`, `prodajni`) VALUES
(73, '2018-06-18', 1, 117.821, 118.175, 118.53),
(74, '2018-06-18', 2, 101.604, 101.91, 102.216),
(75, '2018-06-18', 3, 101.912, 102.219, 102.525),
(76, '2018-06-18', 4, 134.821, 135.227, 135.633),
(77, '2018-06-18', 5, 75.6132, 75.8407, 76.0682),
(78, '2018-06-18', 6, 77.0975, 77.3295, 77.5615),
(79, '2018-06-18', 0, 11.5404, 11.5751, 11.6098),
(80, '2018-06-18', 0, 15.808, 15.8556, 15.9032),
(81, '2018-06-18', 0, 12.4546, 12.4921, 12.5296),
(82, '2018-06-18', 0, 0.919252, 0.922018, 0.924784),
(83, '2018-06-18', 0, 1.6067, 1.6115, 1.6163),
(84, '2018-06-18', 0, 15.775, 15.8225, 15.87);

-- --------------------------------------------------------

--
-- Table structure for table `valuta`
--

DROP TABLE IF EXISTS `valuta`;
CREATE TABLE IF NOT EXISTS `valuta` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(100) NOT NULL,
  `kod` varchar(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `valuta`
--

INSERT INTO `valuta` (`id`, `naziv`, `kod`) VALUES
(1, 'Evropska unija', 'eur'),
(2, 'SAD', 'usd'),
(3, 'Švajcarska', 'chf'),
(4, 'Velika Britanija', 'gbp'),
(5, 'Australija', 'aud'),
(6, 'Kanada', 'cad');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
