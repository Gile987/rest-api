<?php include('db.php'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css">
    <link rel="stylesheet" href="style.css">
    <title>Konvertor Valuta u Dinar</title>
</head>
<body>
  
<div id="container">
    
    <div id="container2">
        <h2>
        Izaberite Kurs
        </h2>

        <form action="convert.php" method="POST" enctype="multipart/form-data">
        
        <div>
            <label class="radio"><input type="radio" name="kurs" value="kupovni" checked>Kupovni</label>
            <label class="radio"><input type="radio" name="kurs" value="srednji">Srednji</label>
            <label class="radio"><input type="radio" name="kurs" value="prodajni">Prodajni</label>
        </div>

        <h2>
        Izaberite Valutu
        </h2>

        <p>
        <select type="text" name="valuta"  value="" required="required">
        <option selected="true" disabled>Valuta</option>;
        
        <?php

        $sql = "SELECT * FROM valuta WHERE kod != '' ORDER BY kod DESC";
        $result = mysqli_query($connection,$sql) or die(mysql_error());

        if (mysqli_num_rows($result)>0) {
        while ($record = mysqli_fetch_array($result,MYSQLI_BOTH))
        echo '<option value="'.$record['id'].'">'.strtoupper($record['kod']).'</option>';
        }

        ?>

        </select>
        </p>

        <h2>
        Unesite Iznos
        </h2>

        <p>
        <input type="number" min="1" name="iznos">
        </p>

        <p>
        <input class="btn" type="submit" value="Convert" name="convert">
        </p>

        </div>

        <div>
        <a href="json.php" target="_blank">Show JSON File</a>
        </div>

        </form>

    </div>

</div>

</body>
</html>